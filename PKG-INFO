Metadata-Version: 2.1
Name: logilab-constraint
Version: 1.0
Summary: constraints satisfaction solver in Python
Home-page: http://www.logilab.org/projects/logilab-constraint
Author: Alexandre Fayolle
Author-email: contact@logilab.fr
License: LGPL
Classifier: Topic :: Scientific/Engineering
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 2
Classifier: Programming Language :: Python :: 3
License-File: COPYING
License-File: COPYING.LESSER
Requires-Dist: setuptools
Requires-Dist: logilab-common<3.0.0,>=2.0.0
Requires-Dist: six>=1.4.0
Requires-Dist: importlib_metadata; python_version < "3.10"

This package implements an extensible constraint satisfaction problem
solver written in pure Python, using constraint propagation algorithms.
The logilab.constraint module provides finite domains with arbitrary
values, finite interval domains, and constraints which can be applied
to variables linked to these domains.

It requires python 2.6 or later to work, and is released under the GNU
Lesser General Public License.

The documentation is in the doc/ directory. Examples are in the
examples/ directory.

Discussion about constraint should take place on the python-projects
mailing list. Information on subscription and mailing list archives can
be accessed at
https://lists.logilab.org/mailman/listinfo/python-projects/

Your feedback is very valuable to us. Please share your experience with
other users of the package on the mailing list.
